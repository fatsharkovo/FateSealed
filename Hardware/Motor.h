#ifndef      __MOTOR_H__
#define      __MOTOR_H__
#include "stm32f4xx.h"                  // Device header


void Motor_Init();
void ESC_Calibration();
void Set_Motor1_Speed(uint16_t speed);
void Set_Motor2_Speed(uint16_t speed);
void Set_Motor3_Speed(uint16_t speed);
void Set_Motor4_Speed(uint16_t speed);









#endif





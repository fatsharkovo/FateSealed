#ifndef      __BLUETOOTH_H__
#define      __BLUETOOTH_H__

#include "stm32f4xx.h"                  // Device header



void BlueTooth_Init();
uint8_t BlueTooth_ReceiveByte();
void BlueTooth_SendByte(uint8_t Byte);
void BlueTooth_SendData(uint32_t Number);

#endif




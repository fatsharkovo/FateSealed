#ifndef      __HMC5883L_H__
#define      __HMC5883L_H__

void HMC5883L_Init(void);
void HMC5883L_Read(uint16_t *data);

#endif



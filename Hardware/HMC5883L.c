#include "stm32f4xx.h"                  // Device header
#include "HMC5883L.h"
#include "IIC.h"



void HMC5883L_Init(void)
{
	//先配置MPU6050寄存器将辅助IIC设备挂载到总线上
	I2C_SEND_BYTE(0xD0,0x6A,0x00);
	I2C_SEND_BYTE(0xD0,0x37,0x02);
	//配置HMC5883L的相关寄存器
	I2C_SEND_BYTE(0x3C,0x00,0x70);
	I2C_SEND_BYTE(0x3C,0x01,0x20);
	I2C_SEND_BYTE(0x3C,0x02,0x00);
}

 

void HMC5883L_Read(uint16_t *data)
{
	uint8_t i;
    uint8_t t[6]; 
	uint8_t addr=0x03;
	//读取状态寄存器，数据准备就绪再进行读取
	while(!(I2C_READ_BYTE(0x3C,0x09)&0x01));
	for(i=0; i<3; i++) 	
	{
		t[2*i]=I2C_READ_BYTE(0x3C,addr);
		t[2*i+1]=I2C_READ_BYTE(0x3C,addr+1);
		data[i]=((t[2*i] << 8)+t[2*i+1]);
		addr+=2;
	}
}































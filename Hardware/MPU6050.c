#include "stm32f4xx.h"                  // Device header
#include "MPU6050.h"
#include "IIC.h"


/**
  * @brief  
  * @param   
  *            @arg 
  * @retval
  */


void MPU6050_Init(void){  //初始化MPU6050
	//I2C_SEND_BYTE(0xD0,0x6B,0x80);//解除休眠状态
	//delay_ms(1000); //等待器件就绪
	
	I2C_SEND_BYTE(0xD0,0x6B,0x00);//解除休眠状态
	I2C_SEND_BYTE(0xD0,0x19,0x00);//陀螺仪采样率
	I2C_SEND_BYTE(0xD0,0x1A,0x06);//中断和低通滤波器
	I2C_SEND_BYTE(0xD0,0x1B,0x18);//陀螺仪自检及测量范围，典型值：0x18(不自检，2000deg/s)	
	I2C_SEND_BYTE(0xD0,0x1C,0x00);//配置加速度传感器工作在2G模式
}



void MPU6050_Read(u16* n){ //读出X、Y、Z三轴加速度/陀螺仪原始数据 //n[0]是AX
	uint8_t i;
    uint8_t t[14];
	uint8_t addr=0x3B;
	for(i=0;i<7;i++)
	{
		t[2*i]=I2C_READ_BYTE(0xD0,addr);
		t[2*i+1]=I2C_READ_BYTE(0xD0,addr+1);
		n[i]=((t[2*i]<<8)+t[2*i+1]);
		addr+=2;
	}
	
}




#include "stm32f4xx.h"                  // Device header



void Serial_Init()
{
	GPIO_InitTypeDef  GPIO_InitStructure;
	RCC_APB2PeriphClockCmd( RCC_APB2Periph_USART1, ENABLE);
	RCC_AHB1PeriphClockCmd( RCC_AHB1Periph_GPIOA, ENABLE);
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10|GPIO_Pin_9; //选择端口号
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF; //选择IO接口工作方式为复用输出  
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz; //设置IO接口速度    
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;//开漏输出
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;//无上拉
	GPIO_Init(GPIOA, &GPIO_InitStructure);
	GPIO_PinAFConfig(GPIOA,GPIO_PinSource9,GPIO_AF_USART1);  //这两个的配置必须有，没有则无法输出
	GPIO_PinAFConfig(GPIOA,GPIO_PinSource10,GPIO_AF_USART1);
	
	USART_InitTypeDef USART_InitStruct;
	USART_InitStruct.USART_BaudRate=9600;
	USART_InitStruct.USART_HardwareFlowControl=USART_HardwareFlowControl_None;
	USART_InitStruct.USART_Mode=USART_Mode_Tx | USART_Mode_Rx;
	USART_InitStruct.USART_Parity=USART_Parity_No;
	USART_InitStruct.USART_StopBits=USART_StopBits_1;
	USART_InitStruct.USART_WordLength=USART_WordLength_8b;
	USART_Init(USART1,&USART_InitStruct);
	USART_Cmd(USART1,ENABLE);
	

}

uint8_t Serial_ReceiveByte()
{
	uint8_t Serial_RxData;
	while(USART_GetFlagStatus( USART1, USART_FLAG_RXNE)==RESET);//此方式为查询，比较占用CPU资源，后面自己改成用中断
	Serial_RxData=USART_ReceiveData(USART1);
	return Serial_RxData;
	
}

void Serial_SendByte(uint8_t Byte)
{
	USART_SendData(USART1,Byte);
	while(USART_GetFlagStatus( USART1, USART_FLAG_TXE)==RESET);
	
}



















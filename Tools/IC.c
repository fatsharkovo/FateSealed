#include "IC.h"


//这部分是用TIM2做的输入捕获
//void IC_Init()
//{
//	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA,ENABLE);
//	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2,ENABLE);
//	
//	GPIO_InitTypeDef GPIO_InitStructure;
//	GPIO_InitStructure.GPIO_Pin=GPIO_Pin_15;//PA15
//	GPIO_InitStructure.GPIO_Speed=GPIO_Speed_50MHz;
//	GPIO_InitStructure.GPIO_Mode=GPIO_Mode_AF;//
//	GPIO_InitStructure.GPIO_PuPd=GPIO_PuPd_UP;
//	GPIO_Init(GPIOA,&GPIO_InitStructure); 
//	GPIO_PinAFConfig(GPIOA, GPIO_PinSource15, GPIO_AF_TIM2);
//	
//	TIM_InternalClockConfig(TIM2);
//	
//	TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStructure;
//	TIM_TimeBaseInitStructure.TIM_ClockDivision = TIM_CKD_DIV1;
//	TIM_TimeBaseInitStructure.TIM_CounterMode = TIM_CounterMode_Up;	//TIM
//	TIM_TimeBaseInitStructure.TIM_Period = 65536-1; //
//	TIM_TimeBaseInitStructure.TIM_Prescaler = 84-1;//
//	TIM_TimeBaseInitStructure.TIM_RepetitionCounter=0;
//	TIM_TimeBaseInit(TIM2, & TIM_TimeBaseInitStructure);

//	TIM_ICInitTypeDef TIM_ICInitStruct;
//	TIM_ICInitStruct.TIM_Channel=TIM_Channel_1;
//	TIM_ICInitStruct.TIM_ICFilter=0xF;
//	TIM_ICInitStruct.TIM_ICPolarity=TIM_ICPolarity_Rising;
//	TIM_ICInitStruct.TIM_ICPrescaler=TIM_ICPSC_DIV1;
//	TIM_ICInitStruct.TIM_ICSelection=TIM_ICSelection_DirectTI;
//	TIM_ICInit(TIM2,&TIM_ICInitStruct);
//	
//	TIM_ICInitStruct.TIM_Channel=TIM_Channel_2;
//	TIM_ICInitStruct.TIM_ICFilter=0xF;
//	TIM_ICInitStruct.TIM_ICPolarity=TIM_ICPolarity_Falling;
//	TIM_ICInitStruct.TIM_ICPrescaler=TIM_ICPSC_DIV1;
//	TIM_ICInitStruct.TIM_ICSelection=TIM_ICSelection_IndirectTI;
//	TIM_ICInit(TIM2,&TIM_ICInitStruct);
//	
//	TIM_SelectInputTrigger(TIM2, TIM_TS_TI1FP1);
//	TIM_SelectSlaveMode(TIM2,TIM_SlaveMode_Reset);
//	
////	TIM_ITConfig(TIM2,TIM_IT_CC1,ENABLE);
////	
////	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
////	NVIC_InitTypeDef NVIC_InitStructure;	
////	NVIC_InitStructure.NVIC_IRQChannel=TIM2_IRQn;
////	NVIC_InitStructure.NVIC_IRQChannelCmd=ENABLE;
////	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority=2;
////	NVIC_InitStructure.NVIC_IRQChannelSubPriority=1;
////	
////	NVIC_Init(&NVIC_InitStructure);
//	
//	TIM_Cmd(TIM2,ENABLE);
//	
//}
 
 
 //使用高级定时器tim1
void IC_Init()
{
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA,ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM1,ENABLE);
	
	GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_InitStructure.GPIO_Pin=GPIO_Pin_8;//PA8
	GPIO_InitStructure.GPIO_Speed=GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode=GPIO_Mode_AF;//
	GPIO_InitStructure.GPIO_PuPd=GPIO_PuPd_UP;
	GPIO_Init(GPIOA,&GPIO_InitStructure); 
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource8, GPIO_AF_TIM1);
	
	//选择内部时钟，不配置也可以
	TIM_InternalClockConfig(TIM1);
	
	//时基单元配置
	TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStructure;
	
	//因为高级定时器有很多要配置的东西，所以用这个函数先把我们不用的
	//东西配置为缺省值，再更改我们需要配置的选项
	TIM_TimeBaseStructInit( & TIM_TimeBaseInitStructure);
	
	TIM_TimeBaseInitStructure.TIM_ClockDivision = TIM_CKD_DIV1;
	TIM_TimeBaseInitStructure.TIM_CounterMode = TIM_CounterMode_Up;	//TIM
	TIM_TimeBaseInitStructure.TIM_Period = 65536-1; //
	TIM_TimeBaseInitStructure.TIM_Prescaler = 84-1;//
	TIM_TimeBaseInitStructure.TIM_RepetitionCounter=0;
	TIM_TimeBaseInit(TIM1, & TIM_TimeBaseInitStructure);
	
	//输入捕获初始化，先是上升沿捕获
	//用的是通道一
	TIM_ICInitTypeDef TIM_ICInitStruct;
	TIM_ICInitStruct.TIM_Channel=TIM_Channel_1;
	TIM_ICInitStruct.TIM_ICFilter=0xF;
	TIM_ICInitStruct.TIM_ICPolarity=TIM_ICPolarity_Rising;
	TIM_ICInitStruct.TIM_ICPrescaler=TIM_ICPSC_DIV1;
	TIM_ICInitStruct.TIM_ICSelection=TIM_ICSelection_DirectTI;
	TIM_ICInit(TIM1,&TIM_ICInitStruct);
	
	//这部分是下降沿捕获，但遥控接收机用ppm的话是不用这个的
	//用的是通道二
	TIM_ICInitStruct.TIM_Channel=TIM_Channel_2;
	TIM_ICInitStruct.TIM_ICFilter=0xF;
	TIM_ICInitStruct.TIM_ICPolarity=TIM_ICPolarity_Falling;
	TIM_ICInitStruct.TIM_ICPrescaler=TIM_ICPSC_DIV1;
	TIM_ICInitStruct.TIM_ICSelection=TIM_ICSelection_IndirectTI;
	TIM_ICInit(TIM1,&TIM_ICInitStruct);
	
	//选择触发源
	//配置从模式，从模式为复位模式
	//这意味着我们检测到上升沿以后，CCR的值会清0
	TIM_SelectInputTrigger(TIM1, TIM_TS_TI1FP1);
	TIM_SelectSlaveMode(TIM1,TIM_SlaveMode_Reset);
	
	//配置中断，通道一，因为通道一我们配置的是上升沿触发，
	//所以检测到上升沿时进入中断
	TIM_ITConfig(TIM1,TIM_IT_CC1,ENABLE);
	
	//中断配置
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
	NVIC_InitTypeDef NVIC_InitStructure;	
	NVIC_InitStructure.NVIC_IRQChannel=TIM1_CC_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelCmd=ENABLE;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority=2;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority=1;
	
	NVIC_Init(&NVIC_InitStructure);
	
	TIM_Cmd(TIM1,ENABLE);
	
}

//测量频率，遥控器接收机不用这个
uint32_t IC_GetFreq()
{
	return 1000000 / (TIM_GetCapture1(TIM1)+1);
}


//获取占空比，遥控器接收机不用这个
uint32_t IC_GetDutyCycle()
{
	return (TIM_GetCapture2(TIM1)+1)*100 / (TIM_GetCapture1(TIM1)+1);
	//return (TIM_GetCapture2(TIM2)+1)*100 / (TIM_GetCapture1(TIM2)+1);
}


//获取CCR的值，在输入捕获模式下，CCR的值代表了时长
//因为输入捕获的频率是1Mhz，所以这个值意味着两次捕获之间的时间间隔（单位：微秒）
uint32_t TeleControl()
{
	return (TIM_GetCapture1(TIM1)+1);	
	//return (TIM_GetCapture1(TIM1)-TIM_GetCapture2(TIM1));
}




//以下是中断函数部分
//其他定时器的相关中断函数也是参照这样的格式
//先判断中断标志位是否是我们想要的中断源置位的
//在函数最后要清除标志位
//void TIM1_CC_IRQHandler()
//{
//	if(TIM_GetITStatus(TIM1,TIM_IT_CC1)==SET)
//	{
//		
//		
//		
//		
//		TIM_ClearITPendingBit(TIM1,TIM_IT_CC1);
//	}
//	
//}






























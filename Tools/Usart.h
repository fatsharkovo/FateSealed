#ifndef      __USART_H__
#define      __USART_H__


void Serial_Init();
uint8_t Serial_ReceiveByte();
void Serial_SendByte(uint8_t Byte);

#endif



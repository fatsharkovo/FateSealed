#ifndef __I2C_H
#define __I2C_H	 

#include "stm32f4xx.h"                  // Device header

void I2C_Configuration(void);
void I2C_SEND_BUFFER(uint8_t SlaveAddr, uint8_t WriteAddr, uint8_t* pBuffer, uint16_t NumByteToWrite);
void I2C_SEND_BYTE(uint8_t SlaveAddr,uint8_t writeAddr,uint8_t pBuffer);
void I2C_READ_BUFFER(uint8_t SlaveAddr,uint8_t readAddr,uint8_t* pBuffer,uint16_t NumByteToRead);
uint8_t I2C_READ_BYTE(uint8_t SlaveAddr,uint8_t readAddr);
uint8_t MY_CHECK_EVENT(uint16_t SR1,uint16_t SR2);

#endif




#include "PWM.h"
#include "Delay.h"

void PWM_Init()
{
	   
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC,ENABLE);
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3,ENABLE);
	//RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2,ENABLE);
	/*  配置GPIO的模式和IO口 */
	GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_InitStructure.GPIO_Pin=GPIO_Pin_6|GPIO_Pin_7|GPIO_Pin_8|GPIO_Pin_9;// PC6
	GPIO_InitStructure.GPIO_Speed=GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode=GPIO_Mode_AF;//复用推挽输出
	GPIO_Init(GPIOC,&GPIO_InitStructure); 
	GPIO_PinAFConfig(GPIOC, GPIO_PinSource6, GPIO_AF_TIM3);
	GPIO_PinAFConfig(GPIOC, GPIO_PinSource7, GPIO_AF_TIM3);
	GPIO_PinAFConfig(GPIOC, GPIO_PinSource8, GPIO_AF_TIM3);
	GPIO_PinAFConfig(GPIOC, GPIO_PinSource9, GPIO_AF_TIM3);
	//TIM3定时器初始化
	TIM_InternalClockConfig(TIM3);
	
	TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStructure;
	TIM_TimeBaseInitStructure.TIM_Period = 199; //PWM 频率=42000000/(199+1)=210Khz//设置自动重装载寄存器周期的值arr
	TIM_TimeBaseInitStructure.TIM_Prescaler = 8399;//设置用来作为TIMx时钟频率预分频值psc
	//TIM_TimeBaseInitStructure.TIM_Period = 2000-1;
	//TIM_TimeBaseInitStructure.TIM_Prescaler = 8399;
	TIM_TimeBaseInitStructure.TIM_ClockDivision = TIM_CKD_DIV1;
	TIM_TimeBaseInitStructure.TIM_CounterMode = TIM_CounterMode_Up;	//TIM向上计数模式
	TIM_TimeBaseInitStructure.TIM_RepetitionCounter=0;
	TIM_TimeBaseInit(TIM3, & TIM_TimeBaseInitStructure);

	//PWM初始化	  //根据TIM_OCInitStruct中指定的参数初始化外设TIMx
	TIM_OCInitTypeDef TIM_OCInitStructure;
	TIM_OCStructInit(&TIM_OCInitStructure);
	TIM_OCInitStructure.TIM_OCMode=TIM_OCMode_PWM1;
	TIM_OCInitStructure.TIM_OCPolarity=TIM_OCPolarity_High;
	TIM_OCInitStructure.TIM_OutputState=TIM_OutputState_Enable;//PWM输出使能
	TIM_OCInitStructure.TIM_Pulse=0;
	
	
	//分别设置四个通道
	TIM_OC1Init(TIM3,&TIM_OCInitStructure);
	TIM_OC1PreloadConfig(TIM3, TIM_OCPreload_Enable);
	TIM_OC2Init(TIM3,&TIM_OCInitStructure);
	TIM_OC2PreloadConfig(TIM3, TIM_OCPreload_Enable);
	TIM_OC3Init(TIM3,&TIM_OCInitStructure);
	TIM_OC3PreloadConfig(TIM3, TIM_OCPreload_Enable);
	TIM_OC4Init(TIM3,&TIM_OCInitStructure);
	TIM_OC4PreloadConfig(TIM3, TIM_OCPreload_Enable);
	
	TIM_ARRPreloadConfig(TIM3,ENABLE);
	TIM_Cmd(TIM3,ENABLE);
}




void PWM2_Init()
{
	   
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC,ENABLE);
	//RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3,ENABLE);
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2,ENABLE);
	/*  配置GPIO的模式和IO口 */
	GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_InitStructure.GPIO_Pin=GPIO_Pin_6|GPIO_Pin_7|GPIO_Pin_8|GPIO_Pin_9;// PC6
	GPIO_InitStructure.GPIO_Speed=GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode=GPIO_Mode_AF;//复用推挽输出
	GPIO_Init(GPIOC,&GPIO_InitStructure); 
	GPIO_PinAFConfig(GPIOC, GPIO_PinSource6, GPIO_AF_TIM2);
	GPIO_PinAFConfig(GPIOC, GPIO_PinSource7, GPIO_AF_TIM2);
	GPIO_PinAFConfig(GPIOC, GPIO_PinSource8, GPIO_AF_TIM2);
	GPIO_PinAFConfig(GPIOC, GPIO_PinSource9, GPIO_AF_TIM2);
	
	//TIM3定时器初始化
	TIM_InternalClockConfig(TIM2);
	
	TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStructure;
	TIM_TimeBaseInitStructure.TIM_Period = 199; //PWM 频率=42000000/(199+1)=210Khz//设置自动重装载寄存器周期的值arr
	TIM_TimeBaseInitStructure.TIM_Prescaler = 8399;//设置用来作为TIMx时钟频率预分频值psc
	//TIM_TimeBaseInitStructure.TIM_Period = 2000-1;
	//TIM_TimeBaseInitStructure.TIM_Prescaler = 8399;
	TIM_TimeBaseInitStructure.TIM_ClockDivision = TIM_CKD_DIV1;
	TIM_TimeBaseInitStructure.TIM_CounterMode = TIM_CounterMode_Up;	//TIM向上计数模式
	TIM_TimeBaseInitStructure.TIM_RepetitionCounter=0;
	TIM_TimeBaseInit(TIM2, & TIM_TimeBaseInitStructure);

	//PWM初始化	  //根据TIM_OCInitStruct中指定的参数初始化外设TIMx
	TIM_OCInitTypeDef TIM_OCInitStructure;
	TIM_OCStructInit(&TIM_OCInitStructure);
	TIM_OCInitStructure.TIM_OCMode=TIM_OCMode_PWM1;
	TIM_OCInitStructure.TIM_OCPolarity=TIM_OCPolarity_High;
	TIM_OCInitStructure.TIM_OutputState=TIM_OutputState_Enable;//PWM输出使能
	TIM_OCInitStructure.TIM_Pulse=0;
	
	TIM_OC1Init(TIM2,&TIM_OCInitStructure);
	TIM_OC1PreloadConfig(TIM2, TIM_OCPreload_Enable);
	TIM_OC2Init(TIM3,&TIM_OCInitStructure);
	TIM_OC2PreloadConfig(TIM3, TIM_OCPreload_Enable);
	TIM_OC3Init(TIM3,&TIM_OCInitStructure);
	TIM_OC3PreloadConfig(TIM3, TIM_OCPreload_Enable);
	TIM_OC4Init(TIM3,&TIM_OCInitStructure);
	TIM_OC4PreloadConfig(TIM3, TIM_OCPreload_Enable);
	
	TIM_ARRPreloadConfig(TIM2,ENABLE);
	TIM_Cmd(TIM2,ENABLE);//使能或者失能TIMx外设
	
}





void Calibration1()
{
	TIM_SetCompare1(TIM3, 20);//10%
	Delay_ms(4000);
	TIM_SetCompare1(TIM3, 10);//5%
	Delay_ms(4000);
}
void Calibration2()
{
	TIM_SetCompare2(TIM3, 180);//10%
	Delay_ms(4000);
	TIM_SetCompare2(TIM3, 190);//5%
	Delay_ms(4000);
}
void Calibration3()
{
	TIM_SetCompare3(TIM3, 180);//10%
	Delay_ms(4000);
	TIM_SetCompare3(TIM3, 190);//5%
	Delay_ms(4000);
}
void Calibration14()
{
	TIM_SetCompare4(TIM3, 180);//10%
	Delay_ms(4000);
	TIM_SetCompare4(TIM3, 190);//5%
	Delay_ms(4000);
}
void Motor1(uint16_t Compare)
{
	TIM_SetCompare1(TIM3, Compare);//0?
}

void Motor2(uint16_t Compare)
{
	TIM_SetCompare2(TIM3, Compare);//0?
}

void Motor3(uint16_t Compare)
{
	TIM_SetCompare3(TIM3, Compare);//0?
}

void Motor4(uint16_t Compare)
{
	TIM_SetCompare4(TIM3, Compare);//0?
}

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
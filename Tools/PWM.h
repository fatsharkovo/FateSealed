#ifndef __PWM_H
#define __PWM_H

#include "stm32f4xx.h"


void PWM_Init(void);
void PWM2_Init(void);
void Motor1(uint16_t Compare);
void Motor2(uint16_t Compare);
void Motor3(uint16_t Compare);
void Motor4(uint16_t Compare);
void Calibration1();
void Calibration2();
void Calibration3();
void Calibration4();




#endif

#ifndef      __IC_H__
#define      __IC_H__
#include "stm32f4xx.h"                  // Device header

void IC_Init();
uint32_t IC_GetFreq();
uint32_t IC_GetDutyCycle();
uint32_t TeleControl();

#endif
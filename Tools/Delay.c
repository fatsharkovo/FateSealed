#include "Delay.h"
 
//static u8  fac_us=0;							//us延时倍乘数			   
//static u16 fac_ms=0;							//ms延时倍乘数
// 
// 
//初始化延迟函数
//SYSTICK的时钟固定为AHB时钟的1/8
//SYSCLK:系统时钟频率
//void SysTick_Init(u8 SYSCLK)
//{
//	SysTick_CLKSourceConfig(SysTick_CLKSource_HCLK_Div8); 
//	fac_us=SYSCLK/8;					
//	fac_ms=(u16)fac_us*1000;				   
//}								    
// 
// 
///*******************************************************************************
//* 函 数 名         : delay_us
//* 函数功能		   : us延时，
//* 输    入         : nus：要延时的us数
//	注意:nus的值,不要大于798915us(最大值即2^24/fac_us@fac_us=21)
//* 输    出         : 无
//*******************************************************************************/		    								   
//void delay_us(u32 nus)
//{		
//	u32 temp;	   
///*
//	#define SysTick             ((SysTick_Type *)       SysTick_BASE)   使能结构体
//	SysTick_BASE就是SysTick_Type类型的结构体指针
//	#define SysTick_BASE        (SCS_BASE +  0x0010)                  !< SysTick Base Address  
//	
//typedef struct	{
//  __IO uint32_t CTRL;                        !< Offset: 0x00  SysTick Control and Status Register 
//  __IO uint32_t LOAD;                        !< Offset: 0x04  SysTick Reload Value Register      
//  __IO uint32_t VAL;                         !< Offset: 0x08  SysTick Current(清空) Value Register      
//  __I  uint32_t CALIB;                       !< Offset: 0x0C  SysTick Calibration Register        
//} SysTick_Type;
//	*/	
//	SysTick->LOAD=nus*fac_us; 					//时间加载	  	nus  	 us是微秒的意思1s=10的六次方微秒
//	SysTick->VAL=0x00;        					//清空计数器
//                          
//	/*#define SysTick_CTRL_ENABLE_Msk   (1ul << SysTick_CTRL_ENABLE_Pos) !< SysTick CTRL: ENABLE Mask    
//  	#define SysTick_CTRL_ENABLE_Pos             0   
//	
//	  !< SysTick CTRL: ENABLE Position 
//	  1ul 表示无符号长整形,最高位不作符号位,1ul在32位计算机中存储二进制为0000 0000 0000 0000 0000 0000 0000 0001
//		
//	  按位进行与运算符‘&’的功能是对参与运算的各对应的二进位相与，9&5：00001001&00000101=00000001.9&5=1
//    按位进行或运算符‘|’的功能是对参与运算的各对应的二进位相或，9|5：00001001|00000101=00001101.9|5=13
//	
//	*/
//	SysTick->CTRL|=SysTick_CTRL_ENABLE_Msk ;	//开始倒数	  
//	do
//	{
//		temp=SysTick->CTRL;
//	}while((temp&0x01)&&!(temp&(1<<16)));		//等待时间到达   
//	SysTick->CTRL&=~SysTick_CTRL_ENABLE_Msk;	//关闭计数器
//	SysTick->VAL =0X00;      					 //清空计数器	 
//}
// 
///*******************************************************************************
//* 函 数 名         : delay_ms
//* 函数功能		   : ms延时，
//* 输    入         : nms：要延时的ms数
//					注意:nms的值,SysTick->LOAD为24位寄存器，
//					不要大于0xffffff*8*1000/SYSCLK
//					对72M条件下,nms<=1864ms 
//* 输    出         : 无
//*******************************************************************************/
//void delay_ms(u16 nms)
//{	 		  	  
//	u32 temp;		   
//	SysTick->LOAD=(u32)nms*fac_ms;				//时间加载(SysTick->LOAD为24bit)
//	SysTick->VAL =0x00;							//清空计数器
//	SysTick->CTRL|=SysTick_CTRL_ENABLE_Msk ;	//开始倒数  
//	do
//	{
//		temp=SysTick->CTRL;
//	}while((temp&0x01)&&!(temp&(1<<16)));		//等待时间到达   
//	SysTick->CTRL&=~SysTick_CTRL_ENABLE_Msk;	//关闭计数器
//	SysTick->VAL =0X00;       					//清空计数器	  	    
//} 
// 

/**
  * @brief  微秒级延时
  * @param  xus 延时时长，范围：0~233015
  * @retval 无
  */
void Delay_us(uint32_t xus)
{
	SysTick->LOAD = 72 * xus;				//设置定时器重装值
	SysTick->VAL = 0x00;					//清空当前计数值
	SysTick->CTRL = 0x00000005;				//设置时钟源为HCLK，启动定时器
	while(!(SysTick->CTRL & 0x00010000));	//等待计数到0
	SysTick->CTRL = 0x00000004;				//关闭定时器
}

/**
  * @brief  毫秒级延时
  * @param  xms 延时时长，范围：0~4294967295
  * @retval 无
  */
void Delay_ms(uint32_t xms)
{
	while(xms--)
	{
		Delay_us(1000);
	}
}
 
/**
  * @brief  秒级延时
  * @param  xs 延时时长，范围：0~4294967295
  * @retval 无
  */
void Delay_s(uint32_t xs)
{
	while(xs--)
	{
		Delay_ms(1000);
	}
}


#ifndef __DELAY_H
#define __DELAY_H

#include "stm32f4xx.h"
 
void SysTick_Init(u8 SYSCLK);
//void delay_ms(u16 nms);
//void delay_us(u32 nus);
void Delay_us(uint32_t xus);
void Delay_s(uint32_t xs);
void Delay_ms(uint32_t xms);
 
 
#endif /* __SYSTICK_H */

#include "stm32f4xx.h"                  // Device header

#include "IIC.h"
#include "PWM.h"
#include "Usart.h"
#include "Delay.h"
#include "IC.h"

#include "OLED.h"
#include "MPU6050.h"
#include "HMC5883L.h"
#include "BlueTooth.h"
#include "Receiver.h"

uint32_t ppm[9];//PPM信号数据
uint16_t mpu6050[7];
uint16_t hmc5883l[3];
uint8_t cnt=0;
uint8_t start=0;
uint16_t ARR=199;
uint16_t PSC=8399;

//GY-86
int main()
{	
	OLED_Init();
	OLED_ShowString(1,14,"D:");
	uint16_t mpu6050[7]={0};
	uint16_t hmc5883l[3]={0};
	I2C_Configuration();
	MPU6050_Init();
	HMC5883L_Init();
	BlueTooth_Init();
	
	//uint16_t low;
	//uint16_t high;
	//uint16_t cnt=0;
	//BlueTooth_SendByte(0x45);
	while(1)
	{
		
		MPU6050_Read(mpu6050);
		HMC5883L_Read(hmc5883l);
		
//		cnt++;
//		if(cnt==10000)
//		{
//			BlueTooth_SendData(mpu6050[1]);
//			cnt=0;
//		}
		
		
		OLED_ShowNum(2,2,mpu6050[0],5);
		OLED_ShowNum(3,2,mpu6050[1],5);
		OLED_ShowNum(4,2,mpu6050[2],5);
		
		//BlueTooth_SendData(mpu6050[0]);
		Delay_ms(500);
		OLED_ShowNum(2,8,hmc5883l[0],5);
		OLED_ShowNum(3,8,hmc5883l[1],5);
		OLED_ShowNum(4,8,hmc5883l[2],5);
		
	}
}

//串口&蓝牙
//int main()
//{
//	
//	//OLED_Init();
//	//OLED_ShowString(1,1,"4564");
//	
//	BlueTooth_Init();
//	Delay_ms(500);
//	
//	
//	while(1)
//	{
//		BlueTooth_SendData(854);
//		Delay_ms(500);
//	}
//}



//电机
//int main()
//{
//	PWM2_Init();
//	
//	TIM_SetCompare1(TIM3, 20);//10%
//	Delay_ms(4000);
//	TIM_SetCompare1(TIM3, 10);//5%
//	Delay_ms(4000);
//	TIM_SetCompare1(TIM3, 11);
//	
//	TIM_SetCompare1(TIM2, 20);//10%
//	Delay_ms(4000);
//	TIM_SetCompare1(TIM2, 10);//5%
//	Delay_ms(4000);
//	TIM_SetCompare1(TIM2, 11);
//	//TIM_SetCompare1(TIM3, 13);
//	//TIM_SetCompare1(TIM3, 14);
//	//TIM_SetCompare1(TIM3, 15);
//	while(1){
//	}
//}



//输入捕获

//int main()
//{
//	//OLED_Init();
//	//OLED_ShowString(1,1,"DutyCycle");
//	
//	PWM_Init();
//	BlueTooth_Init();
//	TIM_SetCompare1(TIM3, 140);   //85%
//	TIM_SetCompare2(TIM3, 100);  //50%
//	TIM_SetCompare3(TIM3, 60);   //85%
//	TIM_SetCompare4(TIM3, 60);   //30%
//	IC_Init();
//	Delay_ms(500);
//	//OLED_ShowNum(2,1,IC_GetFreq(),8);       //50hz
//	//OLED_ShowNum(3,1,IC_GetDutyCycle(),8);  //
//	
//	while(1)
//	{
//		
//		BlueTooth_SendData(IC_GetDutyCycle());
//		Delay_ms(1000);
//		
//		
//		//OLED_ShowNum(2,1,IC_GetFreq(),8);       //50hz
//		//OLED_ShowNum(3,1,IC_GetDutyCycle(),8);  //
//	}
//}


//int main()
//{
//	uint16_t speed;
//	PWM_Init();
//	Calibration1();
//	TIM_SetCompare1(TIM3,11);
//	BlueTooth_Init();
//	IC_Init();
//	Delay_ms(5000);
//	while(1)
//	{
//		//Delay_ms(800);
////		for(int i=0;i<9;i++)
////		{
////			BlueTooth_SendData(ppm[i]);
////			BlueTooth_SendByte((uint8_t)(" "));
////		}
//		//BlueTooth_SendData(ppm[2]);
//		speed =(uint16_t)(ppm[2]/100);
//		if(speed==10)
//		{
//			speed+=1;
//		}
//		TIM_SetCompare1(TIM3, speed);
//	}
//}



////接收机对应要用的中断函数
//void TIM1_CC_IRQHandler()
//{
//	if(TIM_GetITStatus(TIM1,TIM_IT_CC1)==SET)
//	{
//		if(TeleControl()>5000)
//		{
//			start=1;
//		}
//		if(start==1)
//		{
//			ppm[cnt]=TeleControl();
//			cnt++;
//			if(cnt==9)
//			{
//				cnt=0;
//				start=0;
//			}
//		}
//		TIM_ClearITPendingBit(TIM1,TIM_IT_CC1);
//	}
//}





























